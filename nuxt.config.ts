import process from 'node:process'
import { defineNuxtConfig } from 'nuxt/config'
import vuetify, { transformAssetUrls } from 'vite-plugin-vuetify'

export default defineNuxtConfig({
  ssr: false,

  modules: [
    '@unocss/nuxt',
    '@vueuse/nuxt',
    '@pinia/nuxt',
    '@nuxtjs/color-mode',
    '@nuxt/devtools',
    '@vue-macros/nuxt',
    '@nuxtjs/i18n',

    (_options, nuxt) => {
      nuxt.hooks.hook('vite:extendConfig', (config) => {
        config?.plugins?.push(vuetify({ autoImport: true }))
      })
    },
  ],

  runtimeConfig: {
    public: {
      NODE_ENV: process.env.NODE_ENV,
      APP_VERSION: process.env.npm_package_version,
      apiKey: process.env.apiKey,
      authDomain: process.env.authDomain,
      projectId: process.env.projectId,
      storageBucket: process.env.storageBucket,
      messagingSenderId: process.env.messagingSenderId,
      appId: process.env.appId,
      measurementId: process.env.measurementId,
    },
  },

  i18n: {
    strategy: 'no_prefix', // 'prefix_and_default',
    defaultLocale: 'pl',
    vueI18n: './i18n.config.ts',
  },

  components: {
    global: true,
    dirs: ['~/components', '~/models'],
  },

  typescript: {
    strict: true,
    tsConfig: { compilerOptions: { baseUrl: '.' } },
  },

  vite: {
    logLevel: 'info',
    vue: {
      template: {
        transformAssetUrls,
      },
    },
  },

  css: [
    'vuetify/styles',
    '@mdi/font/css/materialdesignicons.css',
    'leaflet/dist/leaflet.css',
  ],

  future: {
    typescriptBundlerResolution: true,
  },

  experimental: {
    writeEarlyHints: true,
  },

  pinia: {
    storesDirs: ['./store/**'],
  },

  macros: {
    betterDefine: false,
  },

  nitro: {
    static: true,
    compressPublicAssets: true,
    esbuild: {
      options: {
        target: 'esnext',
      },
    },
  },
  imports: {
    autoImport: true,
    dirs: ['store'],

  },
  appConfig: {
    buildDate: new Date().toISOString(),
  },

  colorMode: {
    preference: 'system',
  },

  build: {
    transpile: ['vuetify'],
  },

  devtools: {
    enabled: true,

    timeline: {
      enabled: true,
    },
  },

})
