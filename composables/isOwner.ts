export function isOwner() {
  const router = useRouter()
  const userStore = useUserStore()
  const { userData } = storeToRefs(userStore)

  const authStore = useAuthStore()
  const { userAuth } = storeToRefs(authStore)

  watch(userData, (userDataNew) => {
    if ((userAuth.value && !userDataNew) || (userDataNew?.ref && userDataNew?.role !== 'owner')) {
      router.push('/')
    }
  }, { immediate: true })
}
