export function useDialog() {
  const dialog = ref(false)

  const onChangeDialog = (value: boolean) => {
    dialog.value = value
  }
  const onOpenDialog = () => {
    dialog.value = true
  }

  const onCloseDialog = () => {
    dialog.value = false
  }

  return {
    dialog,
    onChangeDialog,
    onOpenDialog,
    onCloseDialog,
  }
}
