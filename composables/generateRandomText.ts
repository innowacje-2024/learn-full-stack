const CHAR_SET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
const SMALL_CHAR_SET = 'abcdefghijklmnopqrstuvwxyz0123456789'

export function generateRandomText(length = 30) {
  return Array(length)
    .fill(CHAR_SET)
    .map((x) => { return x[Math.floor(Math.random() * x.length)] })
    .join('')
}

export function generateRandomTextSmallLetters(length = 30) {
  return Array(length)
    .fill(SMALL_CHAR_SET)
    .map((x) => { return x[Math.floor(Math.random() * x.length)] })
    .join('')
}
