export function useColor() {
  const colorModeRef = ref('light')
  const colorMode = useColorMode()

  watch(colorMode, () => {
    if (colorMode) {
      colorModeRef.value = colorMode?.preference === 'system'
        ? colorMode?.value
        : colorMode?.preference
    }
  }, { immediate: true })
  return {
    colorMode,
    colorModeRef,
  }
}
