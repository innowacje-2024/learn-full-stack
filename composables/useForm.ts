export function useForm() {
  const form: Ref<null | {
    resetValidation: () => void
    reset: () => void
    validate: () => { valid: boolean }
    items: any
  }> = ref(null)

  const isValid = async () => {
    try {
      return (await form.value?.validate())?.valid
    }
    catch (error) {
      return false
    }
  }

  return {
    form,
    isValid,
  }
}
