export function useCreateEditDialog<T>() {
  const dialog: Ref<boolean> = ref(false)
  const itemToEdit: Ref<T | null> = ref(null)

  const onCreate = () => {
    itemToEdit.value = null
    dialog.value = true
  }

  const onEdit = (item: T) => {
    itemToEdit.value = item
    dialog.value = true
  }

  const onChange = (value: boolean) => {
    if (!value)
      itemToEdit.value = null

    dialog.value = value
  }

  const onCloseDialog = () => {
    itemToEdit.value = null
    dialog.value = false
  }

  return {
    dialog,
    itemToEdit,
    onChange,
    onCloseDialog,
    onCreate,
    onEdit,
  }
}
