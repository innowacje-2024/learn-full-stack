import { ImageCompressor } from 'image-compressor'
import type { Ref } from 'vue'
import { ref } from 'vue'

interface ImageCompressorType {
  run: (
    arg0: string | ArrayBuffer | null,
    arg1: {
      toWidth: number
      mimeType: string
      mode: string
      quality: number
      speed: string
    },
    arg2: (data: RequestInfo | URL) => Promise<void>
  ) => void
}

export function useUploadImage() {
  const imageCompressor: Ref<ImageCompressorType> = ref(new ImageCompressor())

  const compressorSettings = {
    mimeType: 'image/webp',
    mode: 'stretch',
    quality: 1,
    speed: 'low',
    toWidth: 1080,
  }

  function toBase64(file: Blob) {
    return new Promise<string | ArrayBuffer | null>((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => resolve(reader.result)
      reader.onerror = error => reject(error)
    })
  }

  function onFileChanged(event: any): Promise<Blob | null> {
    return new Promise((resolve, reject) => {
      if (imageCompressor.value && event?.target?.files[0]) {
        const file = event.target?.files[0]

        const toBlob = (data: RequestInfo | URL) => fetch(data)
          .then(result => result.blob())
          .then(resolve)
          .catch(reject)

        toBase64(file).then((fileInBase64) => {
          imageCompressor.value?.run(
            fileInBase64,
            compressorSettings,
            toBlob,
          )
        })
      }
    })
  }

  return {
    imageCompressor,
    onFileChanged,
    toBase64,
  }
}
