import { type DocumentReference, Timestamp } from 'firebase/firestore'
import type { Meta } from './meta'
import { type Author, toMapAuthor } from './author'
import { type Place, toMapPlace } from './place'

export interface IEvent {
  title: string
  description: string // tekst wyświetlany w liście wydarzeń
  text: string // tekst wyświetlany na stronie wydarzenia
  city: string // miasto
  author: Author // nowy model ze wszystkimi danymi autora
  pubDate: Timestamp // data publikacji
  duration: Timestamp[] // tablica z jedną datą - wydarzenie jednodniowe, z dwiema datami - wydarzenie wielodniowe od do - dodatkowo user moe ustawić godzinę wydarzenia np od 1 maja od 10:00 do 2 maja do 12:00
  places: Place[]
  isPublic: boolean
  edit?: boolean
  imageUrl: string | null
  image: string | null

  ref: DocumentReference | null
}

export interface Event extends IEvent, Meta {
  title: string
  description: string
  text: string
  city: string
  author: Author
  pubDate: Timestamp
  duration: Timestamp[]
  places: Place[]
  isPublic: boolean
  edit?: boolean
  imageUrl: string | null
  image: string | null

  ref: DocumentReference | null

  createdByUser: DocumentReference | null
  lastUpdateByUser: DocumentReference | null
  lastUpdateTime: Timestamp | null
}

export function toMapEvent(data?: Partial<Event>): Event {
  return {
    title: data?.title || '',
    description: data?.description || '',
    text: data?.text || '',
    city: data?.city || '',
    author: toMapAuthor(data?.author),
    pubDate: data?.pubDate ?? Timestamp.now(),
    duration: data?.duration || [],
    places: (data?.places || [])?.map(toMapPlace),
    isPublic: data?.isPublic ?? false,
    imageUrl: data?.imageUrl || null,
    image: data?.image || null,

    ref: data?.ref || null,

    createdByUser: data?.createdByUser || null,
    lastUpdateByUser: data?.lastUpdateByUser || null,
    lastUpdateTime: data?.lastUpdateTime || null,
  }
}

// sample of usage:
// docs.map(mapFromFirebase<Event>(Event))
