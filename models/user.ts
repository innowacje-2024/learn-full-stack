import type { DocumentReference } from 'firebase/firestore'

export interface User {
  uid: string
  role: string
  lang: 'pl' | 'en'

  ref: DocumentReference | null
}

export function toMapUser(data?: Partial<User>): User {
  return {
    uid: data?.uid || '',
    role: data?.role || '',
    lang: data?.lang || 'pl',

    ref: data?.ref || null,
  }
}
