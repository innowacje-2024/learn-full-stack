import { type DocumentReference, GeoPoint, type Timestamp } from 'firebase/firestore'

export interface IPlace {
  name: string // nazwa
  description: string // opis
  address: string // adres w formie tekstowej np: al. Politechniki 11
  photoUrl: string | null
  photoRef: DocumentReference | null
  location: GeoPoint // współrzędne geograficzne
  time: Timestamp[] // tablica z jedną godziną - wydarzenie zaczyna się o tej godzinie, z dwiema godzinami - wydarzenie zaczyna się od pierwszej godziny i kończy po drugiej godzinie
}

export interface Place extends IPlace {
  name: string
  description: string
  address: string
  photoUrl: string | null
  photoRef: DocumentReference | null
  location: GeoPoint
  time: Timestamp[]
}

export function toMapPlace(data?: Partial<Place>): Place {
  return {
    name: data?.name || '',
    description: data?.description || '',
    address: data?.address || '',
    photoUrl: data?.photoUrl || null,
    photoRef: data?.photoRef || null,
    location: data?.location || new GeoPoint(0, 0),
    time: data?.time || [],
  }
}
