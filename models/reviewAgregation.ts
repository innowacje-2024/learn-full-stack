import type { DocumentReference, Timestamp } from 'firebase/firestore'
import type { Meta } from './meta'
import type { Review } from './review'

export interface IReviewAggregation {
  rating: number
  totalRating: number
  reviews: Review[]

  ref: DocumentReference | null
}

export interface ReviewAggregation extends IReviewAggregation, Meta {
  rating: number
  totalRating: number
  reviews: Review[]

  ref: DocumentReference | null

  createdByUser: DocumentReference | null
  lastUpdateByUser: DocumentReference | null
  lastUpdateTime: Timestamp | null
}

export function toMapReviewAggregation(data?: Partial<ReviewAggregation>): ReviewAggregation {
  return {
    rating: data?.rating || 0,
    totalRating: data?.totalRating || 0,
    reviews: data?.reviews || [],

    ref: data?.ref || null,

    createdByUser: data?.createdByUser || null,
    lastUpdateByUser: data?.lastUpdateByUser || null,
    lastUpdateTime: data?.lastUpdateTime || null,
  }
}
