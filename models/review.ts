import { type DocumentReference, Timestamp } from 'firebase/firestore'
import type { Meta } from './meta'
import { type Author, toMapAuthor } from './author'

export interface IReview {
  date: Timestamp
  review: string
  rating: number
  author: Author // nowy model ze wszystkimi danymi autora

  ref: DocumentReference | null
}

export interface Review extends IReview, Meta {
  date: Timestamp
  review: string
  rating: number
  author: Author

  ref: DocumentReference | null

  createdByUser: DocumentReference | null
  lastUpdateByUser: DocumentReference | null
  lastUpdateTime: Timestamp | null

}

export function toMapReview(data?: Partial<Review>): Review {
  return {
    date: data?.date ?? Timestamp.now(),
    review: data?.review || '',
    rating: data?.rating || 0,
    author: toMapAuthor(data?.author),

    ref: data?.ref || null,

    createdByUser: data?.createdByUser || null,
    lastUpdateByUser: data?.lastUpdateByUser || null,
    lastUpdateTime: data?.lastUpdateTime || null,
  }
}

// sample of usage:
// docs.map(mapFromFirebase<Review>(toMapReview))
