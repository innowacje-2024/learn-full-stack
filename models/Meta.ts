import type { DocumentReference, FieldValue, Timestamp } from 'firebase/firestore'

export interface Meta {
  createdByUser: DocumentReference | null
  lastUpdateByUser: DocumentReference | null
  lastUpdateTime: Timestamp | FieldValue | null
}
