import type { DocumentReference } from 'firebase/firestore'

export interface IAuthor {
  name: string
  photoUrl: string | null
  ref: DocumentReference | null
}

export interface Author extends IAuthor {
  name: string
  photoUrl: string | null
  ref: DocumentReference | null
}

export function toMapAuthor(data?: Partial<Author>): Author {
  return {
    name: data?.name || '',
    photoUrl: data?.photoUrl || null,
    ref: data?.ref || null,
  }
}
