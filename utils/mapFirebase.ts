import type { DocumentSnapshot } from 'firebase/firestore'

export function mapFromFirebase<T>(mapFunction: (arg0: any) => any) {
  return (item: DocumentSnapshot): T => ({
    ...mapFunction(item.data()),
    ref: item.ref || null,
  } as T)
}

export function mapToFirebase<T>(object: T) {
  // @ts-expect-error todo
  delete object.ref
  return object
}
