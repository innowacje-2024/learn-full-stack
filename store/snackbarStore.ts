import type { TranslateResult } from 'vue-i18n'

const TIMEOUT = 5000
const TIMEOUT_SHORT = 1400

export const useSnackbarStore = defineStore('snackbar', () => {
  const visible = ref(false)
  const color = ref('primary')
  const text = ref('snackbar.works')
  const timeout = ref(TIMEOUT)

  const resetState = () => {
    visible.value = false
    color.value = 'primary'
    text.value = 'snackbar.works'
    timeout.value = TIMEOUT
  }

  const showSnackbarSaveDefault = () => {
    color.value = 'primary'
    text.value = 'snackbar.saved'
    timeout.value = TIMEOUT
    visible.value = true
    setTimeout(() => {
      timeout.value = TIMEOUT
      visible.value = false
    }, TIMEOUT)
  }

  const showSnackbarCustom = (colorArg: string | null, textArg: string | TranslateResult, customTimeout = TIMEOUT) => {
    color.value = colorArg || 'primary'
    text.value = textArg || 'snackbar.saved'
    timeout.value = customTimeout
    visible.value = true
    setTimeout(() => {
      timeout.value = customTimeout
      visible.value = false
    }, customTimeout)
  }

  const showSnackbarErrorDefault = (errorArg: { code: string } | any) => {
    color.value = 'error'
    text.value = `snackbar.error.${errorArg}`
    timeout.value = TIMEOUT
    visible.value = true
    setTimeout(() => {
      timeout.value = TIMEOUT
      visible.value = false
    }, TIMEOUT)
  }

  const showSnackbarInfinity = (colorArg: string, textArg: string | TranslateResult) => {
    color.value = colorArg || 'primary'
    text.value = textArg || 'snackbar.saved'
    timeout.value = -1
    visible.value = true
  }

  const showSnackbarCopy = () => {
    color.value = 'secondary'
    text.value = 'snackbar.copyClipboard'
    timeout.value = TIMEOUT_SHORT
    visible.value = true
    setTimeout(() => {
      timeout.value = TIMEOUT_SHORT
      visible.value = false
    }, TIMEOUT_SHORT)
  }

  const hideSnackbar = () => {
    timeout.value = TIMEOUT
    visible.value = false
  }

  return {
    visible,
    color,
    text,
    timeout,
    resetState,
    showSnackbarSaveDefault,
    showSnackbarCustom,
    showSnackbarErrorDefault,
    showSnackbarInfinity,
    showSnackbarCopy,
    hideSnackbar,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(
    useSnackbarStore,
    import.meta.hot,
  ))
}
