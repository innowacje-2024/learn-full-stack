interface IError {
  code: string
}

export const useSharedStore = defineStore(
  'shared',
  () => {
    const loading = ref(false)
    const error: Ref<IError | null> = ref(null)

    const $reset = () => {
      error.value = null
      loading.value = false
    }

    const init = () => {
      error.value = null
      loading.value = true
    }

    const success = () => {
      loading.value = false
    }

    const failure = (errorArg: IError) => {
      console.error(errorArg)
      error.value = errorArg
      loading.value = false
    }

    return {
      loading,
      error,

      $reset,
      init,
      success,
      failure,
    }
  },
)

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(
    useSharedStore,
    import.meta.hot,
  ))
}
