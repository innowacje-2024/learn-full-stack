import type { DocumentReference } from 'firebase/firestore'
import { addDoc, collection, doc, getDoc, updateDoc } from 'firebase/firestore'
import type { Review } from '~/models/review'
import type { ReviewAggregation } from '~/models/reviewAgregation'
// import firebase from '~/plugins/firebase'

export const useReviewsStore = defineStore(
  'reviews',
  () => {
    const eventReviews: Ref<ReviewAggregation[]> = ref([])

    const { firestore } = useFirebase()

    const getReviews = async (eventId: string) => {
      const reviewsAggregationRef = doc(firestore, `events/${eventId}/reviews/reviewsAggregation`)
      const reviewsAggregationSnap = await getDoc(reviewsAggregationRef)
      if (reviewsAggregationSnap.exists()) {
        const reviewsAggregation = reviewsAggregationSnap.data() as ReviewAggregation
        const check = eventReviews.value.find(review => review.ref?.id === reviewsAggregation.ref?.id) || null
        if (!check)
          eventReviews.value.push(reviewsAggregation)
      }
    }

    const addReview = async (eventId: DocumentReference, newReview: Review) => {
      const reviewsCollection = collection(eventId, 'reviews')
      const docRef = await addDoc(reviewsCollection, newReview)

      newReview.ref = doc(reviewsCollection, docRef.id)

      const reviewsAggregationRef = doc(reviewsCollection, 'reviewsAggregation')
      const reviewsAggregationSnap = await getDoc(reviewsAggregationRef)

      if (reviewsAggregationSnap.exists()) {
        const reviewsAggregation = reviewsAggregationSnap.data()
        reviewsAggregation.rating += newReview.rating
        reviewsAggregation.totalRating += 1
        reviewsAggregation.reviews.unshift(newReview)

        if (reviewsAggregation.reviews.length > 3)
          reviewsAggregation.reviews.pop()

        await updateDoc(reviewsAggregationRef, reviewsAggregation)

        const check = eventReviews.value.find(review => review.ref?.id === reviewsAggregation.ref.id)
        if (check) {
          const index = eventReviews.value.indexOf(check)
          if (index !== -1)
            eventReviews.value[index] = reviewsAggregation as ReviewAggregation
        }
      }
    }

    return {
      eventReviews,
      getReviews,
      addReview,
    }
  },
)

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(
    useReviewsStore,
    import.meta.hot,
  ))
}
