import { doc, getDoc, setDoc, updateDoc } from 'firebase/firestore'
import { useFirebase } from '~/composables/useFirebase'
import { type User, toMapUser } from '~/models/user'

export const useUserStore = defineStore(
  'user',
  () => {
    const userData: Ref<User | null> = ref(null)
    const $reset = () => {
      userData.value = null
    }

    const { firestore } = useFirebase()
    const { locale } = useI18n()

    const getUser = async (uid: string) => {
      const userRef = doc(firestore, 'users', uid)

      let userSnapshot = await getDoc(userRef)
      if (!userSnapshot.exists()) {
        await setDoc(userRef, {
          role: 'user',
          lang: locale.value,
        })
        userSnapshot = await getDoc(userRef)
      }

      userData.value = mapFromFirebase<User>(toMapUser)(userSnapshot)
      locale.value = userData.value.lang
      userData.value.uid = userSnapshot.id
    }

    const updateLang = async (lang: 'pl' | 'en') => {
      if (!userData.value)
        return

      updateDoc(
        doc(firestore, 'users', userData.value.uid),
        { lang },
      )
      userData.value.lang = lang
      locale.value = lang
    }

    return {
      userData,
      $reset,
      getUser,
      updateLang,
    }
  },
)

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(
    useUserStore,
    import.meta.hot,
  ))
}
