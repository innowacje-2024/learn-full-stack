import type { UserCredential } from 'firebase/auth'
import {
  GoogleAuthProvider,
  deleteUser,
  onAuthStateChanged,
  signInWithPopup,
  signOut,
} from 'firebase/auth'
import { useSharedStore } from './sharedStore'
import { useUserStore } from './userStore'
import { useFirebase } from '~/composables/useFirebase'

export const useAuthStore = defineStore(
  'auth',
  () => {
    const userAuth: Ref = ref()

    const { auth } = useFirebase()
    const router = useRouter()
    const provider = new GoogleAuthProvider()
    const sharedStore = useSharedStore()
    const userStore = useUserStore()
    const $reset = () => {
      userAuth.value = null
    }

    const onSuccessAuthLogin = async ({ user }: UserCredential) => {
      userAuth.value = user
      await userStore.getUser(user.uid)
      sharedStore.success()
    }

    const signUpUserGoogle = () => {
      sharedStore.init()

      signInWithPopup(
        auth,
        provider,
      )
        .then(onSuccessAuthLogin)
        .catch(sharedStore.failure)
    }

    const loginUserGoogle = () => {
      sharedStore.init()

      signInWithPopup(
        auth,
        provider,
      )
        .then(onSuccessAuthLogin)
        .catch(sharedStore.failure)
    }

    const onSuccessCleanData = () => {
      router.push('/')
      $reset()
    }

    const logoutUser = () => {
      sharedStore.init()
      userStore.$reset()
      signOut(auth)
        .then(onSuccessCleanData)
        .catch(sharedStore.failure)
    }

    const deleteUserAction = () => {
      sharedStore.init()

      deleteUser(userAuth.value)
        .then(onSuccessCleanData)
        .catch(sharedStore.failure)
    }

    const currentUser = () => new Promise((resolve, reject) => {
      onAuthStateChanged(
        auth,
        (user) => {
          if (user) {
            sharedStore.init()
            userAuth.value = user
            userStore.getUser(user.uid)
            sharedStore.success()
          }
          resolve(user)
        },
        (error) => {
          reject(error)
        },
      )
    })

    return {
      userAuth,
      $reset,
      signUpUserGoogle,
      loginUserGoogle,
      logoutUser,
      currentUser,
      deleteUserAction,
    }
  },
)

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(
    useAuthStore,
    import.meta.hot,
  ))
}
