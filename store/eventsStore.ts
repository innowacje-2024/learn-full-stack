import type { DocumentData, DocumentReference, Firestore } from 'firebase/firestore'
import { Timestamp, addDoc, collection, deleteDoc, doc, getDoc, getDocs, query, setDoc, updateDoc, where } from 'firebase/firestore'
import { deleteObject, getDownloadURL, ref as refFirebase, uploadBytes } from 'firebase/storage'
import { type Event, toMapEvent } from '~/models/event'
import type { ReviewAggregation } from '~/models/reviewAgregation'

const eventsCollection = (firestore: Firestore) => collection(firestore, 'events')
const publicEventsQuery = (firestore: Firestore) => query(eventsCollection(firestore), where('isPublic', '==', true))

export const useEventStore = defineStore(
  'events',
  () => {
    const events: Ref<Event[]> = ref([])
    const eventsPublic: Ref<Event[]> = ref([])
    const singleEvent: Ref<Event | null> = ref(null)

    const { firestore, storage } = useFirebase()
    const { init, success, failure } = useSharedStore()

    const setImage = async (event: Event, data: Blob) => {
      const storageRef = refFirebase(storage, `img/${event?.ref?.id}/image.webp`)

      const onSuccess = async (snapshot: DocumentData) => {
        const url = await getDownloadURL(snapshot.ref)

        if (event.ref) {
          const data = {
            image: storageRef.fullPath,
            imageUrl: url,
          }

          await updateDoc(
            event.ref,
            data,
          )
          events.value = events.value.map((art: Event) => {
            if (art.ref?.id === event.ref?.id) {
              art.image = storageRef.toString()
              art.imageUrl = url
            }
            return art
          })
        }
      }
      uploadBytes(storageRef, data).then(onSuccess)
    }

    const deleteImage = async (event: Event) => {
      if (event.image && event.imageUrl && event.ref) {
        await deleteObject(refFirebase(storage, event.image))

        event.image = null
        event.imageUrl = null

        await updateDoc(
          event.ref,
          {
            image: null,
            imageUrl: null,
          },
        )

        events.value = events.value.map((art: Event) => {
          if (art.ref?.id === event.ref?.id) {
            art.image = null
            art.imageUrl = null
          }
          return art
        })
      }
    }

    const getEvents = async () => {
      init()
      try {
        const querySnapshot = await getDocs(eventsCollection(firestore))
        const eventsData = querySnapshot.docs.map(mapFromFirebase<Event>(toMapEvent))
        events.value = eventsData
        success()
      }
      catch (error) {
        console.error(error)
        failure({ code: 'get-events' })
      }
    }

    const getEventsPublic = async () => {
      init()
      try {
        const querySnapshot = await getDocs(publicEventsQuery(firestore))
        const eventsData = querySnapshot.docs.map(mapFromFirebase<Event>(toMapEvent))
        eventsPublic.value = eventsData
        success()
      }
      catch (error) {
        console.error(error)
        failure({ code: 'get-events-public' })
      }
    }

    const getEventByUid = async (uid: string) => {
      init()
      try {
        const querySnapshot = await getDoc(doc(firestore, 'events', uid))
        singleEvent.value = mapFromFirebase<Event>(toMapEvent)(querySnapshot)
        success()
      }
      catch (error) {
        console.error(error)
        failure({ code: 'get-event-by-uid' })
      }
    }

    const addEvent = async (newEvent: Event, image: Blob) => {
      init()
      try {
        const docData = toMapEvent(newEvent)
        const docRef = await addDoc(eventsCollection(firestore), mapToFirebase(docData))
        docData.ref = docRef
        events.value.push(docData)

        const newReviewAggregation: ReviewAggregation = {
          rating: 0,
          totalRating: 0,
          reviews: [],
          ref: null,
          createdByUser: null,
          lastUpdateByUser: null,
          lastUpdateTime: Timestamp.now(),
        }

        const reviewsRef = doc(collection(docRef, 'reviews'), 'reviewsAggregation')
        newReviewAggregation.ref = docData.ref
        await setDoc(reviewsRef, newReviewAggregation)

        if (image) {
          await setImage(docData, image)
        }

        success()
      }
      catch (error) {
        console.error(error)
        failure({ code: 'add-event' })
      }
    }

    const updateEvent = async (event: Event, image: Blob) => {
      init()
      if (event.ref) {
        try {
          await updateDoc(event.ref, mapToFirebase(toMapEvent(event)) as any)

          if (image && typeof image !== 'string') {
            await setImage(event, image)
          }
          else if (event.image && !image) {
            await deleteImage(event)
          }
          success()
        }
        catch (error) {
          failure({ code: 'update-event' })
        }
      }
    }

    const deleteEvent = async (eventRef: DocumentReference) => {
      const event = events.value.find(art => art.ref?.id === eventRef.id)

      if (event && event.ref) {
        init()
        try {
          if (event.image) {
            // delete subcollection reviews
            const reviewsRef = collection(event.ref, 'reviews')
            const reviewsQuery = query(reviewsRef)
            const reviewsSnapshot = await getDocs(reviewsQuery)
            reviewsSnapshot.forEach(async (review) => {
              await deleteDoc(review.ref)
            })

            await deleteObject(refFirebase(storage, event.image))
          }
          await deleteDoc(event.ref)
          events.value = events.value.filter(art => art.ref?.id !== eventRef.id)
          success()
        }
        catch (error) {
          failure({ code: 'delete-event' })
        }
      }
    }

    return {
      events,
      eventsPublic,
      singleEvent,
      getEvents,
      getEventsPublic,
      getEventByUid,
      addEvent,
      updateEvent,
      deleteEvent,

      setImage,
      deleteImage,
    }
  },
)

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(
    useEventStore,
    import.meta.hot,
  ))
}
