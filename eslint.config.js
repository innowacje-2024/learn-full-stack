import antfu from '@antfu/eslint-config'

export default antfu({
  stylistic: true,
  typescript: true,
  // vue: true,
  jsonc: true,
  imports: true,
  markdown: true,

  vue: {
    overrides: {
      'vue/max-attributes-per-line': [
        'error',
        {
          singleline: {
            max: 1,
          },
          multiline: {
            max: 1,
          },
        },
      ],
    },
  },
},
)
