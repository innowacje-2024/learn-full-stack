import { pl } from 'vuetify/locale'
import { createVuetify } from 'vuetify'

export default defineNuxtPlugin((app) => {
  const vuetify = createVuetify({
    locale: {
      locale: 'pl',
      fallback: 'pl',
      messages: { pl },
    },
  })
  app.vueApp.use(vuetify)
})
